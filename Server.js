const http = require('http');
const url = require('url');
const insurance = require('./project_insurance_copy');

http.createServer(function (req, res) {
    res.writeHead(200, { 'Content-Type': 'text/html' });
    var query = url.parse(req.url, true).query;
    var txt = query.card + " " + query.customerId + " " + query.reportId + " " + query.fromDate + " " + query.toDate;
    res.write(txt);

    insurance.values_set(query.card, query.customerId, query.reportId,  query.fromDate, query.toDate);
    insurance.getReport().then((report) => { 
        //console.log(report); 
        res.write(report.toString());
        res.end();
    }).catch((error)=>{
        //console.log(error); 
        res.write(error.toString());
        res.end();
    });
}).listen(9000);
