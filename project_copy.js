
var construct = function (patientId, reportId, fromDate, toDate) {
    this.patientId = patientId;
    this.reportId = reportId;
    this.fromDate = fromDate;
    this.toDate = toDate;
}

module.exports.construt = construct;

var reportGenerate = async function () {
    const ComposerClient = require('composer-client');
    const BusinessNetworkConnection = ComposerClient.BusinessNetworkConnection;

    const connection = new BusinessNetworkConnection();
    const businessNetworkDefinition = await connection.connect('admin@hospital');
    connection.on('event', (event) => {
        module.exports.transactionId = event.transactionId;
    });
    
    let serializer = businessNetworkDefinition.getSerializer();
    let resource = serializer.fromJSON({
        '$class': 'org.cusat.hospital.GenerateReport',
        'patient': 'resource:org.cusat.hospital.Patient#' + this.patientId,
        'fromDate': this.fromDate,
        'toDate': this.toDate,
        'reportId': this.reportId,
        'newreport': 'resource:org.cusat.hospital.Report#' + this.reportId

    });
    let report = await connection.submitTransaction(resource);
    connection.disconnect();
    return report;

}
module.exports.reportGenerate = reportGenerate;


var addAck = async function (reportId,transactionId_2) {
    const ComposerClient = require('composer-client');
    const BusinessNetworkConnection = ComposerClient.BusinessNetworkConnection;

    const connection = new BusinessNetworkConnection();
    const businessNetworkDefinition = await connection.connect('admin@hospital');
    let factory = businessNetworkDefinition.getFactory();
    let ackRegistry = await connection.getAssetRegistry('org.cusat.hospital.Acknowledgement');
    let ackAsset= factory.newResource('org.cusat.hospital', 'Acknowledgement', reportId);

    ackAsset.networkName = "Insurance Network";
    ackAsset.transactionId =transactionId_2;
    ackAsset.report = factory.newRelationship('org.cusat.hospital', 'Report', reportId);
    await ackRegistry.add(ackAsset);
    connection.disconnect();

    return ackAsset;

}
module.exports.addAck = addAck;
