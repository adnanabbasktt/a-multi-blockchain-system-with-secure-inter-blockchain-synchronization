
var values_set = function (card, customerId, reportId, fromDate, toDate) {
    this.card = card;
    this.customerId = customerId;
    this.reportId = reportId;
    this.fromDate = fromDate;
    this.toDate = toDate;
}
module.exports.values_set = values_set;


var getReport = async function () {
    const ComposerClient = require('composer-client');
    const BusinessNetworkConnection = ComposerClient.BusinessNetworkConnection;

    const connection = new BusinessNetworkConnection();
    let businessNetworkDefinition_2 = await connection.connect(this.card);

    connection.on('event', (event) => {
        this.transactionId_2 = event.transactionId;
    });


    let reportRegistry = await connection.getAssetRegistry('org.cusat.insurance.Report');
    const hospital = require('./project_copy');
    hospital.construt(this.customerId, this.reportId, this.fromDate, this.toDate);
    let report = await hospital.reportGenerate();
    let factory = businessNetworkDefinition_2.getFactory();
    let serializer = businessNetworkDefinition_2.getSerializer();
    let resource = serializer.fromJSON({
        '$class': 'org.cusat.insurance.SaveReport',
        'patientId': this.customerId,
        'fromDate': this.fromDate,
        'toDate': this.toDate,
        'reportId': this.reportId,
        'summary': report.summary,
        'total': report.total

    });
    let insurancereport = await connection.submitTransaction(resource);



    let ackRegistry = await connection.getAssetRegistry('org.cusat.insurance.Acknowledgement');
    let ackAsset = factory.newResource('org.cusat.insurance', 'Acknowledgement', report.reportId);

    ackAsset.networkName = "Hospital Network";
    ackAsset.transactionId = hospital.transactionId;
    ackAsset.report = factory.newRelationship('org.cusat.insurance', 'Report', insurancereport.reportId);
    await ackRegistry.add(ackAsset);

    let hospitalAck = await hospital.addAck(this.reportId, this.transactionId_2);
    console.log(hospital);

    connection.disconnect();

    return insurancereport;


}
module.exports.getReport = getReport;